import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Fixtures } from './model/fixtures/fixtures';
import { PORT } from './config/configuration';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {cors:true});
  await app.listen(process.env.PORT?parseInt(process.env.PORT): PORT);
}
bootstrap().then(()=>{
  Fixtures.generate();
});
