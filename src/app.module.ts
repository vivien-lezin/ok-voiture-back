import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Car } from './model/entity/car/car.entity';
import { Renter } from './model/entity/renter/renter.entity';
import { CarHttpModule } from './model/entity/car/car-http.module';
import { RenterHttpModule } from './model/entity/renter/renter-http.module';
import { Rent } from './model/entity/rent/rent.entity';
import { RentHttpModule } from './model/entity/rent/rent-http.module';
import { ConfigModule } from '@nestjs/config';
import config from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      load:[config]
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'ok_voiture',
      entities:[Car,Renter,Rent],
      //TODO: à changer à false lors de la mise en production
      dropSchema:true,
      //TODO : à changer à false lors de la mise en production
      synchronize: true,
      //TODO : à changer à false lors de la mise en production
      debug:false
    }),
    CarHttpModule,
    RenterHttpModule,
    RentHttpModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
