export const PORT:number = 4000;

export const URL: string = 'http://localhost:';

export default ()=>({
  port: process.env.PORT? parseInt(process.env.PORT): PORT,
})