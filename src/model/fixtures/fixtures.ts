
import axios from 'axios';
import { Renter } from '../entity/renter/renter.entity';
import { faker } from '@faker-js/faker';
import { Car } from '../entity/car/car.entity';
import { Rent } from '../entity/rent/rent.entity';
import { PORT,URL } from '../../config/configuration';

export class Fixtures {

  private static renters: Renter[] = [];
  private static cars: Car[] = [];
  private static readonly NB_RENTER: number = 20;
  private static readonly NB_RENT_BY_CAR: number = 5;

  public static async generate() {
    this.generateData().then(async () => {


      let cars: Car[] = (await axios.get(URL + PORT + '/car')).data;
      for (const car of cars) {
        const index = cars.indexOf(car);
        let startDate: Date = new Date();
        let endDate: Date = new Date(startDate);
        if (Math.random() >= 0.5)
          for (let i = 0; i < this.NB_RENT_BY_CAR; i++) {
            startDate.setDate(endDate.getDate() + 1);
            endDate.setDate(endDate.getDate() + 10);
            const x = Math.round(Math.random() * this.renters.length);
            let renter:Renter = (Math.random() >= 0.5)?this.createRenter():this.renters[x]
            await this.post('/renter', renter);
            await this.post('/rent', new Rent(
              startDate,
              endDate,
              renter,
              car,
            ));
          }
      }


    });
  }

  private static async post(path: String, data: any) {
    try {
      return await axios.post(URL + PORT + path, data);
    } catch (e) {
      return null;
    }
  }

  private static createCar(renter: Renter): Car {
    return new Car(
      faker.vehicle.manufacturer(),
      faker.vehicle.model(),
      faker.date.past(),
      faker.image.imageUrl(640, 640, 'car'),
      parseInt(faker.finance.amount(50, 200)),
      faker.address.city(),
      renter,
    );
  }

  private static createRenter(): Renter {
    const firstName = faker.name.firstName();
    return new Renter(faker.internet.email(firstName), firstName);
  }

  private static async generateData() {
    for (let i = 0; i < this.NB_RENTER; i++) {
      const renter: Renter = Fixtures.createRenter();
      this.renters.push(renter);
      await this.post('/renter', renter).then(async () => {
        for (let j = 0; j < parseInt(faker.random.numeric()); j++) {
          let car: Car = this.createCar(renter);
          this.cars.push(car);
          await this.post('/car', car);
        }
      });
    }


  }

}