import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Renter } from '../renter/renter.entity';
import { Rent } from '../rent/rent.entity';

@Entity({name:"ov_car"})
export class Car{


  constructor(brand: string, model: string, year: Date, photo:string, priceByDay: number, city: string, renter: Renter) {
    this.brand = brand;
    this.model = model;
    this.year = year;
    this.photo = photo;
    this.priceByDay = priceByDay;
    this.city = city;
    this.renter = renter;
  }

  @PrimaryGeneratedColumn({name:"c_id"})
  id: number;

  @Column({name:"c_brand"})
  brand: string;

  @Column({name:"c_model"})
  model: string;

  @Column({name:"c_year"})
  year: Date;

  @Column({
    name:"c_photo",
    nullable:true
  })
  photo: string;

  @Column({name:"c_price_by_day"})
  priceByDay: number;

  @Column({name:"c_city"})
  city: string;

  @CreateDateColumn({name:"c_created_at"})
  creationCar:Date

  @ManyToOne(() => Renter,{
    nullable:false
  })
  renter: Renter;

  @OneToMany(()=>Rent,rent=>rent.car, {nullable:true})
  rents: Rent[];

}