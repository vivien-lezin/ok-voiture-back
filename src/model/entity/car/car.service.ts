import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Car } from './car.entity';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class CarService{
  constructor(@InjectRepository(Car) private carRepo:Repository<Car>, private dataSource: DataSource) {}

  findAll(): Promise<Car[]>{
    return this.carRepo.find();
  }

  findOne(id:number): Promise<Car|null>{
    return this.carRepo.findOneBy({id});
  }

  async remove(id:number):Promise<void>{
    await this.carRepo.delete(id);
  }

  findAllByPriceAsc(){
    return this.carRepo.find({
      order:{
        priceByDay:"ASC"
      }
    });
  }

  async create(car: Car) {
    await this.dataSource.transaction(async manager =>{
      return await manager.save(car);
    })
  }

  async createMany(cars:Car[]){
    await this.dataSource.transaction(async manager => {
      await cars.forEach(car =>{
        manager.save(car);
      })
    });
  }

  async removeAll(): Promise<void> {
    await this.findAll().then(cars=>{
      cars.forEach(async car =>{
        await this.carRepo.delete(car.id);
      })
    })
  }
}