import { Module } from '@nestjs/common';
import { CarModule } from './car.module';
import { CarService } from './car.service';
import { CarController } from './car.controller';
import { RenterService } from '../renter/renter.service';
@Module({
  imports: [CarModule],
  providers: [CarService,RenterService],
  controllers: [CarController],
})
export class CarHttpModule {}
