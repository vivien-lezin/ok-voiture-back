import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CarService } from './car.service';
import { Car } from './car.entity';
import { Renter } from '../renter/renter.entity';
import { faker } from '@faker-js/faker';
import { RenterService } from '../renter/renter.service';
import { copyFileSync } from 'fs';

@Controller('car')
export class CarController {
  constructor(private carService: CarService, private renterService: RenterService) {
  }


  @Get()
  findAll(){
    return this.carService.findAll();
  }

  @Get(":id")
  findOneBy(@Param() param:string){
    return this.carService.findOne(parseInt(param));
  }

  @Post()
  async create(@Body() car:Car){
    try {
      let renter:Renter|null = await this.renterService.findOne(car.renter.email);
      if (!renter){
        await this.renterService.create(new Renter(
          car.renter.email,
          car.renter.firstName,
        ));
      }
    } catch (e){
      throw new BadRequestException( 'Erreur lors de la création de l\'utilisateur',e);
    }
    try {
      return await this.carService.create(new Car(
        car.brand,
        car.model,
        car.year,
        car.photo,
        car.priceByDay,
        car.city,
        car.renter
      ));
    } catch (e){
      throw new BadRequestException( 'Erreur lors de la création de la voiture',e);
    }

  }

}
