import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';

import { RentService } from './rent.service';
import { Rent } from './rent.entity';
import { RenterService } from '../renter/renter.service';
import { CarService } from '../car/car.service';
import { Renter } from '../renter/renter.entity';

@Controller('rent')
export class RentController {
  constructor(private rentService: RentService, private renterService: RenterService, private carService: CarService) {
  }

  @Post("delete")
  async delete(@Body() rent:Rent) {
    try {
      return await this.rentService.remove(rent.id);
    } catch (e) {
      throw new BadRequestException('Problème suppression location', {
        cause: e,
        description: 'Problème lors de la suppression de la location de voiture',
      });
    }

  }

  @Get()
  findAll() {
    return this.rentService.findAll();
  }

  @Post()
  async create(@Body() rent: Rent) {

    const mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!rent.renter.email.match(mailFormat)) {
      throw new BadRequestException('Pas une adresse email', 'L\'adress email renseignée n\'est pas une adresse email');
    }

    if ((await this.renterService.findOne(rent.renter.email)) === null) {
      throw new BadRequestException('Adresse mail inconnue', 'Il n\'y a pas d\'utilisateur avec cette adresse mail');
    }

    if ((await this.carService.findOne(rent.car.id)) === null) {
      throw new BadRequestException('Voiture inconnue', 'Il n\'y a pas de voiture');
    }

    //Checker la date
    const rents: Rent[] = await this.rentService.findAllRentByIdCar(rent.car.id);
    const rentDateStart: Date = new Date(rent.start);
    const rentDateEnd: Date = new Date(rent.end);
    if (rentDateStart >= rentDateEnd) throw new BadRequestException('Inférieur date départ', 'La date de départ est inférieure à la date d\'arriver');
    rents.forEach(r => {
      if (rentDateStart >= r.start && rentDateStart <= r.end)
        throw new BadRequestException('Date début déjà loué', 'La voiture est déjà louée pour la date de départ');

      if (rentDateEnd >= r.start && rentDateEnd <= r.end)
        throw new BadRequestException('Date de fin déjà loué', 'La voiture est déjà louée pour la date d\'arriver');

      if (rentDateStart <= r.start && rentDateStart <= r.end && rentDateEnd >= r.start && rentDateEnd >= r.end)
        throw new BadRequestException('Mauvaise période', 'La voiture est déjà louée dans cette période');
    });

    try {
      return await this.rentService.create(new Rent(
        rentDateStart,
        rentDateEnd,
        rent.renter,
        rent.car,
      ));
    } catch (e) {
      throw new BadRequestException('Problème création location', {
        cause: e,
        description: 'Problème lors de la création de la location de voiture',
      });
    }
  }

  @Get("order")
  async findOrderByRenterThenCar(){
    return this.rentService.getRentByRenterThenCar();
  }

}
