import { Module } from '@nestjs/common';
import { RentModule } from './rent.module';
import { RentService } from './rent.service';
import { RentController } from './rent.controller';
import { CarModule } from '../car/car.module';
import { RenterService } from '../renter/renter.service';
import { CarService } from '../car/car.service';

@Module({
  imports:[RentModule],
  providers: [RentService,RenterService,CarService],
  controllers:[RentController]
})
export class RentHttpModule {}
