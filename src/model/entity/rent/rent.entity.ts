import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Renter } from '../renter/renter.entity';
import { Car } from '../car/car.entity';

@Entity("ov_rent")
export class Rent{

  constructor(start: Date, end: Date, renter: Renter, car: Car) {
    this.start = start;
    this.end = end;
    this.renter = renter;
    this.car = car;
  }

  @PrimaryGeneratedColumn({ name: 'rt_id' })
  id: number;

  @Column({name:"rt_start"})
  start: Date;

  @Column({name:"rt_end"})
  end: Date;

  @ManyToOne(()=>Renter, renter=>renter.rents, {nullable:false})
  @JoinColumn({name:"rt_fk_r_email"})
  renter:Renter;

  @ManyToOne(()=> Car, car=>car.rents, {nullable:false})
  @JoinColumn({name:"rt_fk_c_id"})
  car:Car;

}