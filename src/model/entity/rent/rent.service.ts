import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Rent } from './rent.entity';
import { DataSource, Repository } from 'typeorm';
import { Renter } from '../renter/renter.entity';
import { Car } from '../car/car.entity';
import { emitKeypressEvents } from 'readline';

@Injectable()
export class RentService {
  constructor(@InjectRepository(Rent) private rentRepo:Repository<Rent>,@InjectRepository(Renter) private renterRepo:Repository<Renter>,@InjectRepository(Car) private carRepo:Repository<Car>, private dataSource:DataSource) {
  }

  findAll(): Promise<Rent[]>{
    return this.rentRepo.find();
  }

  findOne(id:number): Promise<Rent|null>{
    return this.rentRepo.findOneBy({id});
  }

  findAllRentByIdCar(carId:number):Promise<Rent[]>{
    return this.rentRepo.createQueryBuilder("rent")
      .where("rent.car.id = :id",{id:carId})
      .getMany();
  }

  async remove(id:number):Promise<void>{
    await this.rentRepo.delete(id);
  }


  async create(rent: Rent) {
    await this.dataSource.transaction(async manager =>{
      return await manager.save(rent);
    });
  }

  async createMany(rents:Rent[]){
    await this.dataSource.transaction(async manager => {
      await rents.forEach(rent =>{
        manager.save(rent);
      })
    });
  }

  async removeAll(): Promise<void> {
    await this.findAll().then(rents=>{
      rents.forEach(async rent =>{
        await this.rentRepo.delete(rent.id);
      })
    })
  }

  async getRentByRenterThenCar(){

    let data = (await this.renterRepo.find({
      order:{
        email:"ASC"
      },
      relations:{
        rents:true
      }
    }));

    for (const renter of data) {
      const index = data.indexOf(renter);
      for (const rent of renter.rents) {
        const j = renter.rents.indexOf(rent);
        let r = (await this.rentRepo.find({
          where:{
            id: data[index].rents[j].id
          },
          relations:{
            car:true
          }
        }));
        if (r){
          data[index].rents[j].car = r[0].car;
        }
      }
    }

    return data
  }
}
