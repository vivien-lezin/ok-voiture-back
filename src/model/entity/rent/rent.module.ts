import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rent } from './rent.entity';
import { Renter } from '../renter/renter.entity';
import { Car } from '../car/car.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports:[
    ConfigModule,
    TypeOrmModule.forFeature([Rent,Renter,Car])],
  exports:[TypeOrmModule]
})
export class RentModule {

}
