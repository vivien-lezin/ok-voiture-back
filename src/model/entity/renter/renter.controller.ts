import { BadRequestException, Body, Controller, Get, Param, Post } from '@nestjs/common';
import { Renter } from './renter.entity';
import { RenterService } from './renter.service';
import { faker } from '@faker-js/faker';

@Controller('renter')
export class RenterController {
  constructor(private renterService: RenterService) {
  }

  @Get(':email')
  async findOne(@Param("email") param: string) {
    const res = await this.renterService.findOne(param);
    return res;
  }

  @Post()
  async create(@Body() renter: Renter) {
    if ((await this.renterService.findOne(renter.email)) !== null){
      throw new BadRequestException("Déjà un utilisateur",'Il y a déjà un utilisateur avec cette adresse mail');
    }
    return await this.renterService.create(new Renter(renter.email, renter.firstName));
  }
}
