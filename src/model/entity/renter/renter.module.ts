import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Renter } from './renter.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports:[
    ConfigModule,
    TypeOrmModule.forFeature([Renter])],
  exports:[TypeOrmModule]
})
export class RenterModule {

}