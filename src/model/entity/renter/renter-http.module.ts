import { Module } from '@nestjs/common';
import { RenterModule } from './renter.module';
import { RenterService } from './renter.service';
import { RenterController } from './renter.controller';

@Module({
  imports: [RenterModule],
  providers: [RenterService],
  controllers: [RenterController],
})
export class RenterHttpModule {}
