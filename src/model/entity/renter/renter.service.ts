import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Renter } from './renter.entity';
import { DataSource, FindOperator, Repository } from 'typeorm';

@Injectable()
export class RenterService{
  constructor(@InjectRepository(Renter) private renterRepo:Repository<Renter>, protected dataSource: DataSource) {
  }

  findAll(): Promise<Renter[]>{
    return this.renterRepo.find();
  }

  findOne(email:string): Promise<Renter|null>{
    return this.renterRepo.findOneBy({email});
  }

  async remove(email:string):Promise<void>{
    await this.renterRepo.delete(email);
  }

  async create(renter: Renter){
    await this.dataSource.transaction( manager => {
        return manager.save(renter);
      }
    )
  }

  async createMany(renters:Renter[]){
    await this.dataSource.transaction(async manager => {
      await renters.forEach(renter =>{
        manager.save(renter);
      })
    });
  }

  async removeAll(): Promise<void> {
    await this.findAll().then(renters=>{
      renters.forEach(async renter =>{
        await this.renterRepo.delete(renter.email);
      })
    })
  }
}