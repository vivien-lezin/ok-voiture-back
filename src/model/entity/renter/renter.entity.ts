import { Column, CreateDateColumn, Entity, OneToMany, PrimaryColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Car } from '../car/car.entity';
import { Rent } from '../rent/rent.entity';

@Entity({name:"ov_renter"})
export class Renter {


  constructor(email: string, firstName: string) {
    this.email = email;
    this.firstName = firstName;
  }

  @PrimaryColumn({name:"r_email"})
  email:string;

  @Column({name:"r_first_name"})
  firstName: string;

  @CreateDateColumn({name:"r_created_at"})
  createdAt:Date

  @OneToMany(()=>Rent,rent=>rent.renter, {nullable:true})
  rents:Rent[]
}