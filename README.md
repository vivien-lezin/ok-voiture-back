
# Description

[Nest](https://github.com/nestjs/nest) framework TypeScript.

# Lancer l'application

## Configuration SGBDR

Configurez un SGBDR MySQL accessible http://localhost:3600

Si besoin de changer les informations de connexion à la base de données, il faut aller sur [src/app.module.ts](src/app.module.ts) :


```js
// Réinitialiser les paramètres suivants si besoin :
TypeOrmModule.forRoot({
  type: 'mysql',
    host: 'localhost',
  port: 3306,
  username: 'root',
  password: '',
  database: 'ok_voiture',
  entities:[Car,Renter,Rent],
  //TODO: à changer à false lors de la mise en production
  //Pour supprimer la bdd à chaque rédemmarrage du serveur
  dropSchema:true,
  //TODO : à changer à false lors de la mise en production
  //Pour synchronizer la bdd
  synchronize: true,
  //TODO : à changer à false lors de la mise en production
  //Affichage du debugage de TypeORM
  debug:false
})
```

## Lancement de l'app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

Cela lancera l'application et la création des fausses données dans la bdd. Les fausses données sont générées à partir de ce fichier [fixtures.ts](src/model/fixtures/fixtures.ts) 
. Si besoin, allez sur le fichier [src/main.ts](src/main.ts) afin de modifier les informations de connexion à l'API.

```js
export const PORT:number = 4000;

export const URL: string = 'http://localhost:';
```

## Test API

### Voiture

http://localhost:4000/car
****
Get - /

**Récupère toutes les voitures**

***
Get - /[adresseEmail]

**Récupère l'entité attachée Voiture à l'id renseigné**


*****
Post - /

**Créer une nouvelle entité voiture**

* throw BadRequest dans le cas d'une erreur dans la création du loueur
* throw BadRequest dans le cas d'une erreur dans la création de la voiture
***
***
### Location
http://localhost:4000/rent
***
Get - /

**Récupère toutes les locations**

****

Post - /

**Créer une nouvelle location**

Body : L'entité location

* throw BadRequest dans le cas d'une adresse mail en mauvais format 
* throw BadRequest dans le cas où il n'y a pas l'email renseigné dans la bdd 
* throw BadRequest dans le cas où la voiture renseignée n'est pas dans la bdd
* throw BadRequest dans le cas où la date de début est dans une période de location
* throw BadRequest dans le cas où la date de fin est dans une période de location
* throw BadRequest dans le cas où les dates sont dans une période de location
* throw BadRequest dans le cas où d'une erreur lors de la création de la location

****

Post - /delete

**Supprime une entité location**

Body : L'entité location

* throw BadRequest dans le cas où d'une erreur lors de la suppression de la location
***
***
### Loueur

http://localhost:4000/renter
***
Get - /[adresseEmail]

**Récupère l'entité attachée Loueur à l'adresse mail renseignée**

***

Post - /

Body : L'entité Loueur

**Créer un nouveau loueur**

* throw BadRequest dans le cas où un utilisateur à déjà l'email
